def fargs(elem1, *elem2):
    print('elem1 ', elem1)
    print('args ', elem2)


def fkwargs(elem1, **elem2):
    print('elem1 ', elem1)
    print('kwargs ', elem2)


def fargsundkwargs(elem1, *elem2, **elem3):
    print('elem1 ', elem1)
    print('args ', elem2)
    print('kwargs ', elem3)


def person_print(name, last_name, *others, age):
    formatted_data = 'Imię: {}, nazwisko: {}, wiek: {}'.format(name, last_name, age)
    others_str = ' '
    for arg in others:
        others_str += arg + ' '
    print(formatted_data + others_str)


print('funkcja zadana')
person_print('Adam', 'Malysz', 'Mistrz', age='22')
print('funkcja fargs')
fargs('Adam', 'Malysz', 'Mistrz', '22')
print('funkcja fkwargs')
fkwargs('Adam', a='Malysz', b='Mistrz')
print('funkcja fargsundkwargs')
fargsundkwargs('Adam', 'Malysz', 'Mistrz', '22', 'Adam1', a='Malysz1', b='Mistrz1', c='221')
