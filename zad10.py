import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, WebDriverException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

LOGIN = '@op.pl'
PASS = ''

try:
    browser = webdriver.Chrome('chr_dr/chromedriver')
    browser.get('https://manager.paczkomaty.pl/auth/login')
    time.sleep(5)
except (NoSuchElementException, WebDriverException) as e:
    print('startup error:')
    print(e)


def login():
    browser.find_element_by_id("input_login").send_keys(LOGIN)
    browser.find_element_by_id('input_password').send_keys(PASS)
    browser.find_element_by_id('submit_submit').click()
    time.sleep(5)


def deladds():
    browser.find_element_by_css_selector(".d-lg-none > .fas").click()
    time.sleep(5)


def sendpage():
    browser.get('https://manager.paczkomaty.pl/shipments/send/simple')
    time.sleep(5)


def reciverdata():
    browser.find_element_by_id("input_email").send_keys('ab@op.pl')
    browser.find_element_by_id("input_telephone").send_keys('888000001')


def apmparcel():
    radio = browser.find_element_by_id("input_ + parcel_delivery_typestandard_parcel")
    browser.execute_script("arguments[0].click();", radio)


def sizeA():
    radioA = browser.find_element_by_id("input_ + parcel_sizeA")
    browser.execute_script("arguments[0].click();", radioA)
    time.sleep(5)


def receiverapm():
    browser.find_element_by_xpath("//input[@role='combobox']").send_keys('KRA03A')
    time.sleep(10)
    browser.find_element_by_id('input_terminal_option_KRA03A').click()


def sendmetodbyapm():
    browser.find_element_by_xpath("//ng-select[@id='input_send_options']/div/div/div").click()
    browser.find_element_by_xpath("//div[@id='input_send_options_option_EASYPACK']").click()


def acceptregulations():
    browser.find_element_by_id('t4-accept_regulations').click()
    time.sleep(5)


def setcreated():
    browser.find_element_by_xpath("//li/button").click()
    time.sleep(10)


def finaltest():
    url = browser.current_url
    durl = 'https://manager.paczkomaty.pl/shipments/list'
    if url == durl:
        print('done')
    else:
        print('error')
    time.sleep(50)


try:
    login()
except (NoSuchElementException, WebDriverException) as e:
    print('login error:')
    print(e)
else:
    try:
        deladds()
    except (NoSuchElementException, WebDriverException) as e:
        print('deladds error:')
        print(e)
    else:
        try:
            sendpage()
        except (NoSuchElementException, WebDriverException) as e:
            print('sendpage error:')
            print(e)
        else:
            try:
                reciverdata()
            except (NoSuchElementException, WebDriverException) as e:
                print('reciverdata error:')
                print(e)
            else:
                try:
                    apmparcel()
                except (NoSuchElementException, WebDriverException) as e:
                    print('apmparcel error:')
                    print(e)
                else:
                    try:
                        sizeA()
                    except (NoSuchElementException, WebDriverException) as e:
                        print('sizeA error:')
                        print(e)
                    else:
                        try:
                            receiverapm()
                        except (NoSuchElementException, WebDriverException) as e:
                            print('receiverapm error:')
                            print(e)
                        else:
                            try:
                                sendmetodbyapm()
                            except (NoSuchElementException, WebDriverException) as e:
                                print('sendmetodbyapm error:')
                                print(e)
                            else:
                                try:
                                    acceptregulations()
                                except (NoSuchElementException, WebDriverException) as e:
                                    print('acceptregulations error:')
                                    print(e)
                                else:
                                    try:
                                        setcreated()
                                    except (NoSuchElementException, WebDriverException) as e:
                                        print('setcreated error:')
                                        print(e)
                                    else:
                                        try:
                                            finaltest()
                                        except (NoSuchElementException, WebDriverException) as e:
                                            print('finaltest error:')
                                            print(e)
                                        else:
                                            print('Succes')

finally:
    browser.quit()
